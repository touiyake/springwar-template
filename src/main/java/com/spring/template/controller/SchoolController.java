package com.spring.template.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.template.dto.Student;
import com.spring.template.dto.Teacher;
import com.spring.template.layer.service.PersonService;

@RestController
@RequestMapping("/school")
public class SchoolController {

	@Autowired
	private PersonService<Teacher> serviceTeacher;
	
	@Autowired
	private PersonService<Student> serviceStudent;
	
	@PostMapping("/teacher/create")
	public ResponseEntity<Void> createTeacher(@RequestBody Teacher entity) {
		boolean result = this.serviceTeacher.create(entity);
		if (result) {
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/student/create")
	public ResponseEntity<Void> createTeacher(@RequestBody Student entity) {
		boolean result = this.serviceStudent.create(entity);
		if (result) {
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
}