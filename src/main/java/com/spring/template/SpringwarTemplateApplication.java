package com.spring.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringwarTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringwarTemplateApplication.class, args);
	}

}
