package com.spring.template.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "tblteacher")
@Setter @Getter @ToString
public class Teacher implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column private long id;
	@Column private String teacherId;
	@Column private String firstName;
	@Column private String lastName;
	@Column private String middleName;
	
}