package com.spring.template.layer.dao;

import org.springframework.stereotype.Repository;

import com.spring.template.abstracts.AbstractHibernateDao;
import com.spring.template.dto.Teacher;

@Repository
public class TeacherDaoImpl extends AbstractHibernateDao<Teacher>
	implements PersonDao<Teacher> {

	public TeacherDaoImpl() {
		super();
		super.setKlazz(Teacher.class);
	}
	
}