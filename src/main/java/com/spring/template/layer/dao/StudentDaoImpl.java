package com.spring.template.layer.dao;

import org.springframework.stereotype.Repository;

import com.spring.template.abstracts.AbstractHibernateDao;
import com.spring.template.dto.Student;

@Repository
public class StudentDaoImpl extends AbstractHibernateDao<Student> 
	implements PersonDao<Student> {

	public StudentDaoImpl() {
		super();
		super.setKlazz(Student.class);
	}

}