package com.spring.template.layer.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.template.abstracts.AbstractHibernateService;
import com.spring.template.abstracts.BasicOperations;
import com.spring.template.dto.Student;
import com.spring.template.layer.dao.PersonDao;

@Service
@Transactional
public class StudentServiceImpl extends AbstractHibernateService<Student>
	implements PersonService<Student> {

	@Autowired
	private PersonDao<Student> dao;
	
	@Override
	protected BasicOperations<Student> getDao() {
		return this.dao;
	}

	public StudentServiceImpl() {
		super();
	}
	
}
