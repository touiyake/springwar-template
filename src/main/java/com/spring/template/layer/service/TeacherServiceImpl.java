package com.spring.template.layer.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.template.abstracts.AbstractHibernateService;
import com.spring.template.abstracts.BasicOperations;
import com.spring.template.dto.Teacher;
import com.spring.template.layer.dao.PersonDao;

@Service
@Transactional
public class TeacherServiceImpl extends AbstractHibernateService<Teacher>
	implements PersonService<Teacher> {

	@Autowired
	private PersonDao<Teacher> dao;
	
	@Override
	protected BasicOperations<Teacher> getDao() {
		return this.dao;
	}
	
	public TeacherServiceImpl() {
		super();
	}

}
